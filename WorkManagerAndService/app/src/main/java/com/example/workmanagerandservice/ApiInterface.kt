package com.example.workmanagerandservice

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiInterface {
    @GET("posts")
    suspend fun getData(): List<MyDataItem>
}