package com.example.workmanagerandservice

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.workmanagerandservice.databinding.RowItemsBinding

class MyAdapter(): RecyclerView.Adapter<MyAdapter.ViewHolder>(){
    val userList = ArrayList<MyDataItem>()

    class ViewHolder(item: View): RecyclerView.ViewHolder(item) {
        val binding = RowItemsBinding.bind(item)

        fun bind(myDataItem: MyDataItem) = with(binding){
            tvBody.text = myDataItem.body
            tvTitle.text = myDataItem.title
            tvUserID.text = myDataItem.userId.toString()
            tvID.text = myDataItem.id.toString()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var itemView = LayoutInflater.from(parent.context).inflate(R.layout.row_items, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(userList[position])
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    fun addItem(item: MyDataItem){
        userList.add(item)
        notifyDataSetChanged()
    }

    fun clear(){
        userList.clear()
        notifyDataSetChanged()
    }
}