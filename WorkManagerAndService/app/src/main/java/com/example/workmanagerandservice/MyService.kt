package com.example.workmanagerandservice

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MyService: Service() {

    private val TAG = "MyService"

    init {
        Log.i(TAG, "Servise is running...")
    }

    override fun onBind(p0: Intent?): IBinder? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
            .create(ApiInterface::class.java)
        CoroutineScope(Dispatchers.IO).launch {
            Log.i(TAG, "${retrofitBuilder.getData()}")
        }
        return START_REDELIVER_INTENT
    }
}