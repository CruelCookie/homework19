package com.example.workmanagerandservice

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.work.Worker
import androidx.work.WorkerParameters
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MyWorker(context: Context, params: WorkerParameters): Worker(context, params) {

    private val TAG = "MyWorker"

    override fun doWork(): Result {
        try {
            val retrofitBuilder = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
                .create(ApiInterface::class.java)
            CoroutineScope(Dispatchers.IO).launch {
                MainActivity.userList =  retrofitBuilder.getData()
                Log.i(TAG, "Updating")
            }
            return Result.success()
        } catch (e: Exception){
            return Result.failure()
        }
    }
}