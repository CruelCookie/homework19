package com.example.workmanagerandservice

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.work.PeriodicWorkRequest
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import com.example.workmanagerandservice.databinding.ActivityMainBinding
import java.util.Calendar.MINUTE
import java.util.concurrent.TimeUnit

const val BASE_URL = "https://jsonplaceholder.typicode.com/"
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: MyAdapter
    companion object{
        var userList:List<MyDataItem> = listOf()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        adapter = MyAdapter()
        binding.rcView.layoutManager = LinearLayoutManager(applicationContext)
        binding.rcView.adapter = adapter

        setPeriodicGetDataRequest()

        binding.btStartServise.setOnClickListener {
            Intent(this, MyService::class.java).also{
                startService(it)
            }
            binding.tvService.text = "Service running"
        }

        binding.btGetData.setOnClickListener {
            if(adapter.userList.isNotEmpty()){
                adapter.clear()
            }
            for(i in userList){
                adapter.addItem(i)
            }
            Toast.makeText(this, "Will soon update", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setPeriodicGetDataRequest(){
        val workManager = WorkManager.getInstance(applicationContext)
        val periodicWorkRequest = PeriodicWorkRequest.Builder(MyWorker::class.java, 15, TimeUnit.MINUTES).build()
        workManager.enqueue(periodicWorkRequest)
        if(adapter.userList.isNotEmpty()) {
            adapter.clear()
            for (i in userList){
                adapter.addItem(i)
            }
        } else{
            for (i in userList){
                adapter.addItem(i)
            }
        }

    }
}